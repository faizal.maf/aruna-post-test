package id.midnightminer.titleapp.data.repository

import android.util.Log
import id.midnightminer.titleapp.data.local.TitleDao
import id.midnightminer.titleapp.data.local.entity.TitleDb
import id.midnightminer.titleapp.data.remote.ApiService
import id.midnightminer.titleapp.data.response.TitleResponse
import id.midnightminer.titleapp.utils.ViewState
import id.midnightminer.titleapp.utils.extensions.httpError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Midnightminer on 17/01/22.
 */
interface TitleRepository {
    fun getTitlePost(): Flow<ViewState<List<TitleDb>>>

    suspend fun getTitlePostApi(): Response<TitleResponse>
}

@Singleton
class DefaultTitleRepository @Inject constructor(
    private val titleDao: TitleDao,
    private val apiService: ApiService
): TitleRepository, TitleMapper {
    override fun getTitlePost(): Flow<ViewState<List<TitleDb>>> = flow {
        emit(ViewState.loading())

        val freshTitle = getTitlePostApi()
        freshTitle.body()?.titles?.toStorage()?.let(titleDao::clearAndCacheTitles)

        val cachedTitles = titleDao.getTitles()
        emitAll(cachedTitles.map { ViewState.success(it) })
    }
        .flowOn(Dispatchers.IO)

    override suspend fun getTitlePostApi(): Response<TitleResponse> {
        return try {
            apiService.getAllPosts()
        } catch (e: Exception) {
            httpError(404)
        }
    }
}