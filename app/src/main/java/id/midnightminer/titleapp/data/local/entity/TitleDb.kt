package id.midnightminer.titleapp.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import id.midnightminer.titleapp.data.local.entity.TitleDb.Titles.tableName
import id.midnightminer.titleapp.data.local.entity.TitleDb.Titles.Column

/**
 * Created by Midnightminer on 17/01/22.
 */
@Entity(tableName = tableName)
data class TitleDb(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name= Column.title)
    val title: String? = null
) {
    object Titles {
        const val tableName = "titles"
        object Column {
            const val id = "id"
            const val title = "title"
        }
    }
}