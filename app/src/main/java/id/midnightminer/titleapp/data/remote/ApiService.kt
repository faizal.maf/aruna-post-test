package id.midnightminer.titleapp.data.remote

import id.midnightminer.titleapp.data.response.TitleResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * Created by Midnightminer on 17/01/22.
 */

interface ApiService {
    @GET("/posts")
    suspend fun getAllPosts(): Response<TitleResponse>
}