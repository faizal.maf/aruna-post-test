package id.midnightminer.titleapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import id.midnightminer.titleapp.data.local.entity.TitleDb
import kotlinx.coroutines.flow.Flow

/**
 * Created by Midnightminer on 17/01/22.
 */
@Dao
interface TitleDao {
    @Insert
    fun insertTitle(titles: List<TitleDb>): List<Long>

    @Query("SELECT * FROM titles")
    fun getTitles(): Flow<List<TitleDb>>

    @Query("DELETE FROM titles")
    fun clearAllTitles()

    @Transaction
    fun clearAndCacheTitles(titles: List<TitleDb>) {
        clearAllTitles()
        insertTitle(titles)
    }
}