package id.midnightminer.titleapp.data.repository

import id.midnightminer.titleapp.data.local.entity.TitleDb
import id.midnightminer.titleapp.data.model.Title
import id.midnightminer.titleapp.data.remote.ApiService

/**
 * Created by Midnightminer on 17/01/22.
 */
interface TitleMapper : Mapper<TitleDb, Title> {
    override fun TitleDb.toRemote(): Title {
        return Title(
            id = id,
            title = title
        )
    }

    override fun Title.toStorage(): TitleDb {
        return TitleDb(
            id = id,
            title = title
        )
    }
}