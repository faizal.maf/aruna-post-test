package id.midnightminer.titleapp.data.response

import com.google.gson.annotations.SerializedName
import id.midnightminer.titleapp.data.model.Title

/**
 * Created by Midnightminer on 17/01/22.
 */
data class TitleResponse(
    @SerializedName("")
    val titles: List<Title> = emptyList()
)