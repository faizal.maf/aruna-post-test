package id.midnightminer.titleapp.data.local

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Created by Midnightminer on 17/01/22.
 */
abstract class TitleDatabase: RoomDatabase() {
    abstract fun titleDao(): TitleDao

    companion object {
        private const val databaseName = "titles-db"

        fun buildDefault(context: Context) =
            Room.databaseBuilder(context, TitleDatabase::class.java, databaseName)
                .build()
    }
}