package id.midnightminer.titleapp.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Midnightminer on 17/01/22.
 */
data class Title(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String?,
)