package id.midnightminer.titleapp.di

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import id.midnightminer.titleapp.data.local.TitleDatabase
import javax.inject.Singleton

/**
 * Created by Midnightminer on 17/01/22.
 */
@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun provideDb(app: Application): TitleDatabase = TitleDatabase.buildDefault(app)

}