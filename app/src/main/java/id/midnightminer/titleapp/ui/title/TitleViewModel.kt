package id.midnightminer.titleapp.ui.title

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.midnightminer.titleapp.data.local.entity.TitleDb
import id.midnightminer.titleapp.data.repository.TitleRepository
import id.midnightminer.titleapp.utils.ViewState

/**
 * Created by Midnightminer on 17/01/22.
 */
class TitleViewModel @ViewModelInject constructor(
    titleRepository: TitleRepository): ViewModel() {

    private val titleDb: LiveData<ViewState<List<TitleDb>>> = titleRepository.getTitlePost().asLiveData()

    fun getTitles(): LiveData<ViewState<List<TitleDb>>> = titleDb
}