package id.midnightminer.titleapp.ui.title

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.midnightminer.titleapp.databinding.ActivityTitleBinding
import id.midnightminer.titleapp.utils.ViewState
import id.midnightminer.titleapp.utils.extensions.observeNotNull

/**
 * Created by Midnightminer on 17/01/22.
 */
class TitleActivity: AppCompatActivity() {
    private val titleViewModel: TitleViewModel by viewModels()

    private lateinit var binding: ActivityTitleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTitleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = TitleAdapter()
        binding.rvTitle.adapter = adapter
        binding.rvTitle.layoutManager = LinearLayoutManager(this)

        titleViewModel.getTitles().observeNotNull(this) { state ->
            when (state) {
                is ViewState.Success -> adapter.submitList(state.data)
                is ViewState.Error -> Toast.makeText(this,"Something went wrong", Toast.LENGTH_SHORT).show()
            }
        }
    }

}