package id.midnightminer.titleapp.ui.title

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.midnightminer.titleapp.R
import id.midnightminer.titleapp.data.local.entity.TitleDb
import id.midnightminer.titleapp.databinding.ItemTitleBinding

/**
 * Created by Midnightminer on 18/01/22.
 */
class TitleAdapter() : ListAdapter<TitleDb, TitleAdapter.TitleViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TitleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_title,parent,false))


    override fun onBindViewHolder(newsHolder: TitleViewHolder, position: Int) = newsHolder.bind(getItem(position))

    class TitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding = ItemTitleBinding.bind(itemView)

        fun bind(titles: TitleDb) = with(itemView) {
            binding.tvTitle.text = titles.title
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TitleDb>() {
            override fun areItemsTheSame(oldItem: TitleDb, newItem: TitleDb): Boolean = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: TitleDb, newItem: TitleDb): Boolean = oldItem == newItem
        }
    }
}