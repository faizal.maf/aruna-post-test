package id.midnightminer.titleapp.utils.extensions

import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response
import retrofit2.Retrofit

/**
 * Created by Midnightminer on 18/01/22.
 */
inline fun <reified T> Retrofit.create(): T = create(T::class.java)

fun <T> httpError(code: Int): Response<T> = Response.error<T>(code, "".toResponseBody(null))